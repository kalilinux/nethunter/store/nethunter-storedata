! Ensure /data is not encrypted, see https://forum.xda-developers.com/android/software/universal-dm-verity-forceencrypt-t3817389
* Roll back change to resolve "key not required" issue caused by encrypted data partition due to sporadic undesired consequences, such as "socket: Permission  denied" errors. 
* Migrate hardcoded "kali-armhf" rootfs name to "kalifs" symlink